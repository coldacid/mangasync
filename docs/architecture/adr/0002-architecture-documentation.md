# 2. Architecture documentation

Date: 2018-11-01

## Status

Accepted

## Context

Architecture documentation, including architecture decision records, must be stored in a plain text format:

* This works well with version control systems.

* This also works well with tools intended to help manage documentation and system architecture.

* It allows the tool to modify the status of records and insert hyperlinks when one decision supercedes another.

* Decisions can be read in the terminal, IDE, version control browser, etc.

Developers will want to use some formatting: lists, code examples, and so on.

People will want to view the architecture documentation in a more readable format than plain text, and maybe print them out.

## Decision

Record architecture decisions in [Markdown format](https://daringfireball.net/projects/markdown/).

Use [PlantUML](https://plantuml.com/) for architecture diagrams.

Document the architecture and related issues using the "software guidebook" structure from Simon Brown's [Software Architecture for Developers: Volume 2](https://leanpub.com/visualising-software-architecture).

## Consequences

Architecture documentation can be read in the terminal.

Documentation will be formatted nicely and hyperlinked by the browsers of project hosting sites like GitHub and Bitbucket.

Tools like [Pandoc](https://pandoc.org/) can be used to convert documentation into HTML or PDF.
