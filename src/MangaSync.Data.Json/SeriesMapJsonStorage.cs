﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MangaSync.Data.Json.Converters;
using MangaSync.Data.Json.Models;
using MangaSync.Data.Json.Options;
using MangaSync.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace MangaSync.Data.Json
{
    public class SeriesMapJsonStorage
    {
        private readonly SeriesMap _seriesMap;
        private readonly JsonStorageOptions _options;
        private readonly ILogger<SeriesMapJsonStorage> _logger;

        internal ICollection<SeriesData> Series => _seriesMap.Series;

        internal IDictionary<string, MangaService> Trackers => _seriesMap.Trackers;

        internal IDictionary<string, MangaService> Aggregators => _seriesMap.Aggregators;

        public SeriesMapJsonStorage(IOptions<JsonStorageOptions> options, ILogger<SeriesMapJsonStorage> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            if (!Path.IsPathRooted(_options.MappingsFile))
            {
                _options.MappingsFile = Path.Combine(_options.StorageDirectory, _options.MappingsFile);
            }

            _seriesMap = LoadFile(_options.MappingsFile);
        }

        internal SeriesMap LoadFile(string filePath)
        {
            JObject json = null;
            var seriesMap = new SeriesMap();

            try
            {
                using (var reader = File.OpenText(filePath))
                    json = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
            }
            catch (FileNotFoundException)
            {
                _logger.LogWarning("User mappings file {0} doesn't exist, returning empty series map", filePath);
                seriesMap.Series = new List<SeriesData>();
                seriesMap.Trackers = new Dictionary<string, MangaService>();
                seriesMap.Aggregators = new Dictionary<string, MangaService>();
                return seriesMap;
            }

            var series = json["Series"];
            seriesMap.Series = series.Select(s => GetSeriesData((JObject)s)).ToList();

            var trackers = (JObject)json["Trackers"];
            seriesMap.Trackers = trackers.Properties()
                .Select(t => GetMangaService(ServiceType.Tracker, t.Name, (JObject)t.Value))
                .ToDictionary(t => t.Id, t => t);

            var aggregators = (JObject)json["Aggregators"];
            seriesMap.Aggregators = aggregators.Properties()
                .Select(a => GetMangaService(ServiceType.Aggregator, a.Name, (JObject)a.Value))
                .ToDictionary(a => a.Id, a => a);

            return seriesMap;
        }

        private static SeriesData GetSeriesData(JObject obj)
        {
            var series = new SeriesData
            {
                Id = Guid.TryParse((string)obj["Id"], out var guid) ? guid : Guid.NewGuid(),
                DefaultTitle = (string)obj["DefaultTitle"],
                Author = obj["Author"] != null ? (string)obj["Author"] : null,
                Status = Enum.TryParse<SeriesStatus>((string)obj["Status"], out var status)
                    ? status
                    : new SeriesStatus?()
            };

            if (obj["Title"] is JObject titles && titles != null)
            {
                foreach (var title in titles.Properties())
                    series.Title[title.Name] = (string)title.Value;
            }

            if (obj["AlternateTitles"] is JObject altTitles && altTitles != null)
            {
                foreach (var title in altTitles.Children().SelectMany(c => c.Children()).OfType<JProperty>())
                    series.AlternateTitles.Add(new LocalizedString(title.Name, (string)title.Value));
            }

            if (obj["Trackers"] is JObject trackers && trackers != null)
            {
                foreach (var tracker in trackers.Properties())
                    series.Trackers[tracker.Name] = (string)tracker.Value;
            }

            if (obj["Aggregators"] is JObject aggregators && aggregators != null)
            {
                foreach (var property in aggregators.Properties())
                {
                    if (property.Value is JArray array)
                        series.Aggregators[property.Name] = property.Value.Select(v => (string)v).ToList();
                    else
                        series.Aggregators[property.Name] = new List<string> { (string)property.Value };
                }
            }

            return series;
        }

        private static MangaService GetMangaService(ServiceType type, string key, JObject obj)
        {
            var service = new MangaService
            {
                Type = type,
                Id = key,
                Name = (string)obj["Name"],
                Home = new Uri((string)obj["Home"], UriKind.Absolute),
                Language = (string)obj["Language"],
                UrlMask = (string)obj["UrlMask"],
            };

            if (obj["AltMasks"] is JArray altMasks && altMasks != null)
            {
                foreach (var mask in altMasks)
                    service.AlternativeMasks.Add((string)mask);
            }

            return service;
        }

        internal static void SaveFile(string filePath, SeriesMap map)
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new List<JsonConverter>
                {
                    new AggregatorsListJsonConverter(),
                    new LocalizedStringJsonConverter(),
                    new StringEnumConverter()
                },
                ContractResolver = new SeriesMapContractResolver()
            };
            var serializer = JsonSerializer.Create(settings);

            using (var sw = new StreamWriter(filePath))
            using (var jwriter = new JsonTextWriter(sw))
            {
                serializer.Serialize(jwriter, map);
            }
        }
    }
}
