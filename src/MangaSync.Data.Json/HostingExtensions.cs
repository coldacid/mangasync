using System;
using MangaSync.Data.Json.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MangaSync.Data.Json
{
    public static class HostingExtensions
    {
        public static IServiceCollection AddJsonRepositories(this IServiceCollection services, IConfigurationRoot config)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            return services
                .Configure<JsonStorageOptions>(options =>
                {
                    var section = config?.GetSection("MangaSync.Data.Json");
                    var defaults = new JsonStorageOptions();

                    if (section != null)
                    {
                        options.MappingsFile = section[nameof(JsonStorageOptions.MappingsFile)] ?? defaults.MappingsFile;
                        options.ProgressFile = section[nameof(JsonStorageOptions.ProgressFile)] ?? defaults.ProgressFile;
                        options.StorageDirectory = section[nameof(JsonStorageOptions.StorageDirectory)] ?? defaults.StorageDirectory;
                    }
                    else
                    {
                        options.MappingsFile = defaults.MappingsFile;
                        options.ProgressFile = defaults.ProgressFile;
                        options.StorageDirectory = defaults.StorageDirectory;
                    }
                })
                .AddSingleton<SeriesMapJsonStorage>()
                .AddSingleton<ProgressMapJsonStorage>()
                .AddTransient<IAggregatorsRepository, AggregatorsRepository>()
                .AddTransient<IProgressRepository, ProgressRepository>()
                .AddTransient<ISeriesRepository, SeriesRepository>()
                .AddTransient<ITrackersRepository, TrackersRepository>();
        }

        public static IServiceCollection AddJsonRepositories(this IServiceCollection services) =>
            AddJsonRepositories(services, null);
    }
}