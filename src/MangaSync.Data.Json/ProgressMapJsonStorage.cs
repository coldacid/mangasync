using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MangaSync.Data.Json.Converters;
using MangaSync.Data.Json.Models;
using MangaSync.Data.Json.Options;
using MangaSync.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace MangaSync.Data.Json
{
    public class ProgressMapJsonStorage
    {
        private readonly IDictionary<Guid, SeriesProgress> _progressMap;
        private readonly JsonStorageOptions _options;
        private readonly ILogger<ProgressMapJsonStorage> _logger;

        internal IDictionary<Guid, SeriesProgress> Progress => _progressMap;

        public ProgressMapJsonStorage(IOptions<JsonStorageOptions> options, ILogger<ProgressMapJsonStorage> logger)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            if (!Path.IsPathRooted(_options.ProgressFile))
            {
                _options.ProgressFile = Path.Combine(_options.StorageDirectory, _options.ProgressFile);
            }

            _progressMap = LoadFile(_options.ProgressFile);
        }

        internal IDictionary<Guid, SeriesProgress> LoadFile(string filePath)
        {
            JObject json = null;

            try
            {
                using (var reader = File.OpenText(filePath))
                    json = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
            }
            catch (FileNotFoundException)
            {
                _logger.LogWarning("User progress file {0} doesn't exist, returning empty progress data", filePath);
                return new Dictionary<Guid, SeriesProgress>();
            }

            var progress = json["Progress"];
            if (progress == null || progress.Type != JTokenType.Array)
            {
                _logger.LogError("User progress file {0} does not match expected schema, missing Progress array", filePath);
                throw new ApplicationException($"Bad schema for {filePath}");
            }

            return progress
                .Select(p => GetProgress((JObject)p))
                .ToDictionary(p => p.SeriesId, p => p);
        }

        private static SeriesProgress GetProgress(JObject obj)
        {
            var progress = new SeriesProgress
            {
                SeriesId = Guid.TryParse((string)obj["SeriesId"], out var guid) ? guid : Guid.NewGuid()
            };

            if (obj["FirstRead"] is JValue firstRead && firstRead != null && firstRead.Type != JTokenType.Null)
                progress.FirstRead = (DateTime)firstRead;
            if (obj["LastRead"] is JValue lastRead && lastRead != null && lastRead.Type != JTokenType.Null)
                progress.LastRead = (DateTime)lastRead;
            if (obj["LatestChapter"] is JValue latestChapter && latestChapter != null && latestChapter.Type == JTokenType.Float)
                progress.LatestChapter = (decimal)latestChapter;

            return progress;
        }

        internal static void SaveFile(string filePath, IEnumerable<SeriesProgress> progress)
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include
            };
            var serializer = JsonSerializer.Create(settings);

            using (var sw = new StreamWriter(filePath))
            using (var jwriter = new JsonTextWriter(sw))
            {
                serializer.Serialize(jwriter, new { Version = "0.1.0", Progress = progress });
            }
        }
    }
}
