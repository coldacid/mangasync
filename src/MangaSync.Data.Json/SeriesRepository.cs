using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Data;
using MangaSync.Models;

namespace MangaSync.Data.Json
{
    public class SeriesRepository : ISeriesRepository
    {
        private readonly SeriesMapJsonStorage _storage;

        public SeriesRepository(SeriesMapJsonStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Add(SeriesData item)
        {
            throw new NotImplementedException();
        }

        public void Delete(SeriesData item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SeriesData> GetAll() => _storage.Series.AsEnumerable();

        public SeriesData GetById(Guid id) => _storage.Series.SingleOrDefault(s => s.Id == id);

        public void Update(SeriesData item)
        {
            throw new NotImplementedException();
        }
    }
}