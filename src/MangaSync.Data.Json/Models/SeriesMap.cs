using System;
using System.Collections.Generic;
using MangaSync.Models;

namespace MangaSync.Data.Json.Models
{
    public class SeriesMap
    {
        public IList<SeriesData> Series { get; set; }

        public IDictionary<string, MangaService> Trackers { get; set; }

        public IDictionary<string, MangaService> Aggregators { get; set; }
    }
}