using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MangaSync.Data.Json.Converters
{
    public class LocalizedStringJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => typeof(LocalizedString).IsAssignableFrom(objectType);

        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            var obj = JObject.Load(reader);

            var prop = obj.Children().OfType<JProperty>().FirstOrDefault();
            if (prop == null)
                return null;

            return new LocalizedString(prop.Name, (string)prop.Value);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is LocalizedString locString)) return;

            var obj = new JObject();
            obj.Add(locString.Language, locString.Value);
            serializer.Serialize(writer, obj);
        }
    }
}