using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MangaSync.Data.Json.Converters
{
    public class AggregatorsListJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) =>
            typeof(IDictionary<string, IList<string>>).IsAssignableFrom(objectType);

        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            var obj = JObject.Load(reader);
            var props = obj.Children().OfType<JProperty>();

            var dict = new Dictionary<string, IList<string>>();
            foreach (var p in props)
            {
                dict[p.Name] = ParseValue(p.Value);
            }

            return dict;
        }

        private IList<string> ParseValue(JToken value)
        {
            if (value is JArray array)
            {
                var list = new List<string>(array.Count);
                foreach (var token in array)
                    list.Add((string)token);
                return list;
            }
            else
                return new List<string> { (string)value };
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is IDictionary<string, IList<string>> dict)) return;

            var obj = new JObject();
            foreach (var pair in dict)
                obj.Add(pair.Key, MakeValue(pair.Value));
            serializer.Serialize(writer, obj);
        }

        private JToken MakeValue(IList<string> list)
        {
            if (list.Count == 1)
                return new JValue(list[0]);

            var array = new JArray();
            foreach (var s in list)
                array.Add(new JValue(s));
            return array;
        }
    }
}