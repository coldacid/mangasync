using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Data;
using MangaSync.Models;

namespace MangaSync.Data.Json
{
    public class AggregatorsRepository : IAggregatorsRepository
    {
        private readonly SeriesMapJsonStorage _storage;

        public AggregatorsRepository(SeriesMapJsonStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Add(MangaService item)
        {
            throw new NotImplementedException();
        }

        public void Delete(MangaService item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MangaService> GetAll() => _storage.Aggregators.Values.AsEnumerable();

        public MangaService GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Update(MangaService item)
        {
            throw new NotImplementedException();
        }
    }
}