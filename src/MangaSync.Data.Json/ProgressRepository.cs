using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Data;
using MangaSync.Models;

namespace MangaSync.Data.Json
{
    public class ProgressRepository : IProgressRepository
    {
        private readonly ProgressMapJsonStorage _storage;

        public ProgressRepository(ProgressMapJsonStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Add(SeriesProgress item)
        {
            throw new NotImplementedException();
        }

        public void Delete(SeriesProgress item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SeriesProgress> GetAll() => _storage.Progress.Values;

        public SeriesProgress GetById(Guid id) => _storage.Progress.TryGetValue(id, out var p) ? p : null;

        public void Update(SeriesProgress item)
        {
            throw new NotImplementedException();
        }
    }
}
