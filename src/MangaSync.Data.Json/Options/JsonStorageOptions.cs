using System;
using System.IO;

namespace MangaSync.Data.Json.Options
{
    public class JsonStorageOptions
    {
        public string StorageDirectory { get; set; } =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".mangasync");

        public string MappingsFile { get; set; } = "mappings.json";

        public string ProgressFile { get; set; } = "progress.json";
    }
}