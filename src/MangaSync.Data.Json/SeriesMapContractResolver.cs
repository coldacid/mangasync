using System;
using System.Collections.Generic;
using System.Linq;
using MangaSync.Data.Json.Converters;
using MangaSync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MangaSync.Data.Json
{
    internal class SeriesMapContractResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var properties = base.CreateProperties(type, memberSerialization);

            if (typeof(MangaService).IsAssignableFrom(type))
            {
                properties = properties
                    .Where(p => p.PropertyName != nameof(MangaService.Id) && p.PropertyName != nameof(MangaService.Type))
                    .ToList();
            }

            if (typeof(SeriesData) == type)
            {
                var aggs = properties.Single(p => p.PropertyName == nameof(SeriesData.Aggregators));
                aggs.Converter = new AggregatorsListJsonConverter();
            }

            return properties;
        }
    }
}