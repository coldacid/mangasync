using System;
using System.Linq;
using MangaSync.Data;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Logging;

namespace MangaSync.Commands.List
{
    [Command(Description = "Lists manga progress")]
    [HelpOption]
    class ListCommand
    {
        private readonly ISeriesRepository _series;
        private readonly IProgressRepository _progress;
        private readonly IConsole _console;
        private readonly ILogger<ListCommand> _logger;

        public ListCommand(
            ISeriesRepository series,
            IProgressRepository progress,
            IConsole console,
            ILogger<ListCommand> logger)
        {
            _series = series ?? throw new ArgumentNullException(nameof(series));
            _progress = progress ?? throw new ArgumentNullException(nameof(progress));
            _console = console ?? throw new ArgumentNullException(nameof(console));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        private void OnExecute()
        {
            var series = _series.GetAll();
            if (!series.Any())
            {
                Console.WriteLine("No series available!");
                return;
            }

            foreach(var manga in series)
            {
                _console.WriteLine("{0}", manga.DefaultTitle);

                var progress = _progress.GetById(manga.Id);
                if (progress != null)
                {
                    _console.WriteLine("    Last read chapter {0} on {1:D}", progress.LatestChapter, progress.LastRead);
                }
                else
                {
                    _console.WriteLine("    No chapters have been read!");
                }

                _console.WriteLine();
            }
        }
    }
}