﻿using System;
using System.IO;
using System.Linq;
using MangaSync.Commands.List;
using MangaSync.Data.Json;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using SpecialFolder = System.Environment.SpecialFolder;

namespace MangaSync
{
    [Command(Name = "mangasync", Description = "Manage and synchronize manga progress across trackers and apps")]
    [Subcommand("list", typeof(ListCommand))]
    class Program
    {
        public static int Main(string[] args)
        {
            var basePath = Path.Combine(Environment.GetFolderPath(SpecialFolder.UserProfile), ".mangasync");
            Directory.CreateDirectory(basePath);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("config.json", optional: true)
                .Build();

            var services = new ServiceCollection()
                .AddOptions()
                .AddLogging(builder =>
                {
                    builder.AddDebug();
                })
                .AddSingleton<IConsole>(PhysicalConsole.Singleton)
                .AddJsonRepositories(configuration)
                .BuildServiceProvider();

            var app = new CommandLineApplication<Program>();
            app.Conventions
                .UseDefaultConventions()
                .UseConstructorInjection(services);

            return app.Execute(args);
        }
    }
}
