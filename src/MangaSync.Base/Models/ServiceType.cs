using System;

namespace MangaSync.Models
{
    public enum ServiceType
    {
        Aggregator = 1,

        Tracker = 2
    }
}