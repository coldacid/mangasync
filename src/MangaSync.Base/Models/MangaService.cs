using System;
using System.Collections.Generic;

namespace MangaSync.Models
{
    public class MangaService
    {
        public ServiceType Type { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public Uri Home { get; set; }

        public string Language { get; set; }

        public string UrlMask { get; set; }

        public IList<string> AlternativeMasks { get; } = new List<string>();
    }
}