using System;

namespace MangaSync.Models
{
    public class MangaSource : IEquatable<MangaSource>
    {
        public string Name { get; set; }

        public Uri Uri { get; set; }

        public override int GetHashCode()
        {
            return (Name?.GetHashCode() ?? 0) ^ (Uri?.GetHashCode() ?? 0);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as MangaSource);
        }

        public bool Equals(MangaSource other)
        {
            if (ReferenceEquals(other, null)) return false;
            return (this.Name == other.Name && this.Uri == other.Uri);
        }
    }
}