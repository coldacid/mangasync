using System;
using System.Collections.Generic;

namespace MangaSync.Models
{
    public class SeriesData
    {
        public Guid Id { get; set; }

        public string DefaultTitle { get; set; }

        public IDictionary<string, string> Title { get; } = new Dictionary<string, string>();

        public IList<LocalizedString> AlternateTitles { get; } = new List<LocalizedString>();

        public string Author { get; set; }

        public SeriesStatus? Status { get; set; }

        public IDictionary<string, string> Trackers { get; } = new Dictionary<string, string>();

        public IDictionary<string, IList<string>> Aggregators { get; } = new Dictionary<string, IList<string>>();
    }
}