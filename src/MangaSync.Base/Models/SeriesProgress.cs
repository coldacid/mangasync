using System;

namespace MangaSync.Models
{
    public class SeriesProgress
    {
        public Guid SeriesId { get; set; }

        public DateTime? FirstRead { get; set; }

        public DateTime? LastRead { get; set; }

        public decimal? LatestChapter { get; set; }
    }
}