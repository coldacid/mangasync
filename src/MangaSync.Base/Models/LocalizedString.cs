using System;

namespace MangaSync.Models
{
    public class LocalizedString
    {
        /// <summary>
        /// Gets or sets the BCP-47 language tag for the string value.
        /// </summary>
        /// <value>A language tag in the format specified by BCP-47.</value>
        public string Language { get; } = "xx";

        /// <summary>
        /// Gets or sets the value of the localized string.
        /// </summary>
        /// <value>The value of the localized string.</value>
        public string Value { get; }

        public LocalizedString(string language, string value)
        {
            if (String.IsNullOrWhiteSpace(language))
                throw new ArgumentNullException(nameof(language));

            Language = language;
            Value = value;
        }

        public LocalizedString(string value) : this("xx", value) {}
    }
}