using System;

namespace MangaSync.Models
{
    public enum SeriesStatus
    {
        Ongoing = 1,

        Completed = 2
    }
}