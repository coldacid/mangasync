using System;
using System.Collections.Generic;

namespace MangaSync.Data
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetById(Guid id);

        void Add(T item);

        void Update(T item);

        void Delete(T item);
    }
}