using System;
using System.Collections.Generic;
using MangaSync.Models;

namespace MangaSync.Data
{
    public interface ISeriesRepository : IRepository<SeriesData>
    {
    }
}